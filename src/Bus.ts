import { IVehicle } from "./IVehicule"

/**
 * Bus is slower than a {@link Car}
 * @see Your Bus engeenir to build more faster Bus
 * @public
 */
export class Bus implements IVehicle{

  public allowedEveryWhere: boolean = false
  private isCollector : boolean

  constructor (
    /***/
    public schoolBus: boolean,
    public price: number,
    public passengers : number,
    public weels: number,
    public oneMoreParams: Record<string, any>,
    public owner?: string
  ) {
    this.isCollector = price > 2000000
  }

}
