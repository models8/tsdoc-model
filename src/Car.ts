import { IVehicle } from "./IVehicule"

/**
 * Car is fast faster than {@link Bus}
 * @see Car vs Bus on youtube ...
 * @public
 */
export class Car implements IVehicle{

  public allowedEveryWhere: boolean = true

  constructor (
    public price: number,
    public passengers : number,
    public weels: number,
    public owner?: string
  ) {}
}
