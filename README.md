

# TSDoc

TsDoc is jsDoc based **convention** for typescript (with additionnal tags for decorators, class, ...)


# TypeDoc

It's documentation generator for Typescript + TsDoc
  - Can generate doc only from typescript 
  - Can generate doc from typescript + TsDoc


**Install**
- ```sh 
  npm i -D typedoc
  ```

**Config file** 
- `typedoc.json` or `typedoc.js`

Documentation  [here](https://typedoc.org/guides/overview/)

# ESLINT PLUGIN TSDOC

Documentation linting

**Install**
- ```sh
  npm i -D eslint-plugin-tsdoc
  ```

**Config** 

- Add it in esling config file
- Configuration guide  [here](https://www.npmjs.com/package/eslint-plugin-tsdoc)
