/**
 * An interface describing a Vehicle.
 * @public
 */
export interface IVehicle {
  /** Price on dollar to bus*/
  price: number
  passengers : number
  weels : number
  allowedEveryWhere: boolean
  owner? : string
}
