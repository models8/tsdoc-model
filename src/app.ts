export { Car } from "./Car"
export { Bus } from "./Bus"
export { IVehicle } from "./IVehicule"
/**
 * A test function that do nothing
 * @param quantity - Number of product to buy
 * @param productName - Name of product
 * @beta
 */
export const run = (quantity: number, productName: string) : string => {
  return `Il y a ${quantity}  ${productName}`
}
